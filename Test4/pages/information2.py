# _*_ coding:utf-8 _*_
import time

from appium.webdriver.common.mobileby import MobileBy

from UINT6.homework.pages.delete_member_page import Delete_Page
from UINT6.zhibo.pages.base_page import BasePage


class Information2(BasePage):

    def goto_DeleteMember_page(self):
        self.find(MobileBy.XPATH,"//*[@text='编辑成员']").click()
        time.sleep(1)
        return Delete_Page(self.driver)
