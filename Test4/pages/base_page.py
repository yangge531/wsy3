
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
import logging
root=logging.getLogger()
print(root.handlers)
for h in root.handlers[:]:
    root.removeHandler(h)

class BasePage:
    logging.basicConfig(level=logging.INFO,
                    filename='../logtxt/new.log',
                    filemode='a',##模式，有w和a，w就是写模式，每次都会重新写日志，覆盖之前的日志
                    #a是追加模式，默认如果不写的话，就是追加模式
                    format='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s')
                    #日志格式)
    def __init__(self,driver:WebDriver=None):
        self.driver=driver

    def find(self,by,value):

        logging.info(by)
        logging.info(value)

        return self.driver.find_element(by,value)

    def swipe_find(self,text,num=3):
        # num=3
        for i in range(0,num):
            if i==num-1:
                raise NoSuchElementException(f"找了{num-1}次,没有找到元素")
            try:
                return self.find(MobileBy.XPATH,f"//*[@text='{text}']")
            except:
                print("未找到,滑动")
                size=self.driver.get_window_size()
                width=size['width']
                height=size['height']
                startx=width/2
                starty=height*0.8
                endx=startx
                endy=height*0.3
                duration=2000#2s
                self.driver.swipe(startx,starty,endx,endy)
