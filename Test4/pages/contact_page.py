# _*_ coding:utf-8 _*_
from time import sleep

from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from UINT6.homework.pages.base_page import BasePage
from UINT6.homework.pages.information_page import Information



class ContactListPage(BasePage):

    def goto_Information(self,username):
        #click[添加成员]
        self.find(MobileBy.ANDROID_UIAUTOMATOR,'new UiScrollable(new UiSelector()'
                                                              '.scrollable(true).instance(0)).'
                                                              'scrollIntoView(new UiSelector().'
                                                              f'text("{username}").instance(0));').click()
        return Information(self.driver)
    def find_member(self,name):
        self.find(MobileBy.XPATH, "//*[@bounds='[648,41][729,122]']").click()
        self.find(MobileBy.XPATH,"//*[@text='搜索']").send_keys(name)
        sleep(2)
        if self.find(MobileBy.XPATH,"//*[@text='无搜索结果']"):
            print("删除成员成功")
