# _*_ coding:utf-8 _*_
from appium.webdriver.common.mobileby import MobileBy

from UINT6.homework.pages.base_page import BasePage


class Delete_Page(BasePage):

    def goto_contact_page(self):
        from UINT6.homework.pages.contact_page import ContactListPage
        self.find(MobileBy.ID,"com.tencent.wework:id/e37").click()
        self.find(MobileBy.ID,"com.tencent.wework:id/bei").click()
        return ContactListPage(self.driver)
