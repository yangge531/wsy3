# _*_ coding:utf-8 _*_
from appium.webdriver.common.mobileby import MobileBy


from UINT6.zhibo.pages.base_page import BasePage

from UINT6.homework.pages.information2 import Information2
class Information(BasePage):

    def goto_information2(self):

        self.find(MobileBy.ID,"com.tencent.wework:id/h8g").click()
        return Information2(self.driver)
