#首页的封装
# _*_ coding:utf-8 _*_
from appium.webdriver.common.mobileby import MobileBy

from UINT6.homework.pages.base_page import BasePage
from UINT6.homework.pages.contact_page import ContactListPage


class IndexPage(BasePage):
    def goto_contactlist(self):
        #click通讯录

        self.find(MobileBy.XPATH,"//*[@text='通讯录']").click()
        return ContactListPage(self.driver)