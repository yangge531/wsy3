# _*_ coding:utf-8 _*_
from appium import webdriver
from UINT6.homework.pages.Index_page import IndexPage
from UINT6.homework.pages.base_page import BasePage


class App(BasePage):
    def start(self):
        if self.driver==None:
            #driver=空就创建driver
            print("driver==None,创建driver")

            caps={}
            caps["platformName"]="Android"
            caps["deviceName"]='127.0.0.1:7555'
            caps['appPackage']='com.tencent.wework'
            caps['appActivity']='.launch.LaunchSplashActivity'
            #防止清空缓存信息-比如登录信息
            caps['noReset']='true'
            #最重要的一步，与server建立连接
            self.driver=webdriver.Remote("http://localhost:4723/wd/hub",caps)
            #隐式等待5秒
            self.driver.implicitly_wait(5)
        else:
            print("复用driver")
            self.restart()
        return self

    def restart(self):
        self.driver.close_app()
        self.driver.launch_app()

    def stop(self):
        self.driver.quit()

    def goto_main(self):
        return IndexPage(self.driver)

