
# import sys
# sys.path.append('..')
# import sys
# sys.path.append('..')
import os
import sys
from time import sleep

import yaml
currentpath=os.path.abspath(__file__)
rootpath=os.path.dirname(currentpath)
zz=os.path.dirname(os.path.dirname(os.path.dirname(rootpath)))
sys.path.append(zz)
from UINT6.homework.pages.app import App
from UINT6.homework.pages.base_page import BasePage


# def get_datas():
#     with open("../pages/delete_llist.yaml", encoding="utf-8") as f:
#         datas = yaml.safe_load(f)
#         print(datas)
#     return datas
class TestContact:
    def setup_class(self):
        #打开页面，进入到首页
        self.app=App()


    def setup(self):
        self.main = self.app.start().goto_main()
    def teardown_class(self):
        self.app.stop()


    # @pytest.mark.parametrize('names',get_datas()['namelist']['names'])
    def test_delete1(self):
        with open("../pages/delete_llist.yaml", encoding="utf-8") as f:
            datas = yaml.safe_load(f)
        names=datas['namelist']['names']

        # print(name)
        self.main.goto_contactlist().goto_Information(names[0]).goto_information2().goto_DeleteMember_page().goto_contact_page().find_member(names[0])

    def test_delete2(self):
        with open("../pages/delete_llist.yaml", encoding="utf-8") as f:
            datas = yaml.safe_load(f)
        names=datas['namelist']['names']

        # print(name)
        self.main.goto_contactlist().goto_Information(names[1]).goto_information2().goto_DeleteMember_page().goto_contact_page().find_member(names[1])

    def test_delete3(self):
        with open("../pages/delete_llist.yaml", encoding="utf-8") as f:
            datas = yaml.safe_load(f)
        names=datas['namelist']['names']

        # print(name)
        self.main.goto_contactlist().goto_Information(names[2]).goto_information2().goto_DeleteMember_page().goto_contact_page().find_member(names[2])