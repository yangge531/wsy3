"""
__author__ = 'hogwarts_xixi'
__time__ = '2021/4/2 8:05 下午'
"""
import allure
import pytest
import yaml


# pip install pyyaml
def get_datas():
    with open("./datas/calc.yml",encoding='utf-8') as f:
        datas = yaml.safe_load(f)
    return datas


@allure.feature("测试计算器的用例")
class TestCal:


    @allure.story("测试相加功能")
    @pytest.mark.parametrize('a,b,expect', get_datas()['add']['datas'], ids=get_datas()['add']['ids'])
    def test_add(self, calculate, a, b, expect):
        with allure.step("计算两个数相加"):
            res=calculate.add(a,b)
        with allure.step('比较结果'):
            assert expect==res

    @allure.story("测试相减功能")
    @pytest.mark.parametrize('a,b,expect', get_datas()['subtrac']['datas'], ids=get_datas()['subtrac']['ids'])
    def test_subtrac(self, calculate, a, b, expect):
        with allure.step("计算两个数相减"):
            res = calculate.subtrac(a, b)
            if isinstance(res, float):
                res = round(res, 3)
        with allure.step('比较结果'):
            assert expect == res

    @allure.story("测试相乘功能")
    @pytest.mark.parametrize('a,b,expect', get_datas()['mult']['datas'], ids=get_datas()['mult']['ids'])
    def test_mult(self, calculate, a, b, expect):
        with allure.step("计算两个数相乘"):
            res = calculate.mult(a, b)
            if isinstance(res, float):
                res = round(res, 3)
        with allure.step('比较结果'):
            assert expect == res

    @allure.story("测试相除功能")
    @pytest.mark.parametrize('a,b,expect', get_datas()['div']['datas'], ids=get_datas()['div']['ids'])
    def test_div(self, calculate, a, b, expect):
        with allure.step("计算两个数相除"):
            res = calculate.div(a, b)
            if isinstance(res, float):
                res = round(res, 3)
        with allure.step('比较结果'):
            assert expect == res

    # def test_div(self, calculate):
    #     # try:
    #     #     cal.div(1, 0)
    #     # except ZeroDivisionError:
    #     #     print("除数为0")
    #     with pytest.raises(ZeroDivisionError):
    #         calculate.div(1, 0)