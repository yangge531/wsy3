from selenium.webdriver.common.by import By

from homework3.zuoye.page.base_page import BasePage
from homework3.zuoye.page.contact_page import Contact


class AddDepartPage(BasePage):
    def add_depart(self,name):
        #点击
        #点击部门输入框并且输入内容
        self.driver.find_element_by_css_selector("[name='name']").send_keys(name)
        #点击选择所属部门下拉框
        self.driver.find_element_by_class_name("js_parent_party_name").click()
        #点击阳光科技的定位，id前加上弹窗的定位
        self.driver.find_element(By.CSS_SELECTOR,".qui_dialog_body.ww_dialog_body [id='1688850148925509_anchor']").click()
        #点击确定按钮
        self.driver.find_element_by_link_text("确定").click()
        #返回通讯录页面的对象
        return Contact(self.driver)

    def goto_contact(self):
        pass
