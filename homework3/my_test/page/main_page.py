from selenium import webdriver
from selenium.webdriver.common.by import By

from homework3.zuoye.page.add_depart_page import AddDepartPage
from homework3.zuoye.page.base_page import BasePage
from time import sleep
from selenium.webdriver.common.action_chains import ActionChains

class MainPage(BasePage):
    #通讯录定位
    add_member_ele = (By.ID, "menu_contacts")
    #通讯录下的加号定位
    add_depart_ele = (By.CSS_SELECTOR,".member_colLeft_top_addBtnWrap.js_create_dropdown")
    #通讯录下的添加部门定位
    add_depart_ele2=(By.CSS_SELECTOR,".js_create_party")
    def goto_add_depart(self):
        #使用浏览器复用模式

        self.find(self.add_member_ele).click()
        sleep(3)
        self.find(self.add_depart_ele).click()
        self.find(self.add_depart_ele2).click()
        #返回添加部门页面的对象
        return AddDepartPage(self.driver)



    def goto_contact(self):
        pass