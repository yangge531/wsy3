from selenium.webdriver.common.by import By

from homework3.zuoye.page.base_page import BasePage
from time import sleep

class Contact(BasePage):
    def goto_add_member(self):
        pass

    def get_list(self):
        #部门列表
        departlist=[]
        sleep(3)
        #部门列表的定位
        ele_list = self.driver.find_elements(By.CSS_SELECTOR, ".jstree-node.js_editable.jstree-leaf")
        for i in ele_list:
            #获取部门列表的部门名称，并且去除空格
            b=i.text.strip()
            departlist.append(b)
        return departlist
