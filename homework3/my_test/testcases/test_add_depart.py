from homework3.zuoye.page.main_page import MainPage
from time import sleep

class TestAddDepart:
    def test_add_member(self):
    #用来测试添加成员功能
        self.main=MainPage()
    #1.跳转到添加成员页面 2.添加成员3.获取成员列表，做断言验证
        # name="研发部"
        #
        # res = self.main.goto_add_depart().add_depart(name).get_list()
        # sleep(2)
        # assert name in res
        #部门列表
        namelist=["视频部","旅游部","电商部","游戏部"]
        #遍历部门列表，进行添加操作
        for name in namelist:
             res = self.main.goto_add_depart().add_depart(name).get_list()
             sleep(3)
        sleep(2)
        #验证部门列表的部门都被添加成功
        for name2 in namelist:
            assert name2 in res